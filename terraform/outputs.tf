output "kubeconfig" {
  value       = aws_eks_cluster.eks_cluster.endpoint
  description = "EKS cluster endpoint"
}