/label ~"SupDeVinci | Todo"

**Labels à appliquer : 🏷️ `SupDeVinci | Equipe X`**

# Titre de l'Issue

## 📝 Description
Décrivez brièvement le problème ou la fonctionnalité demandée.

## 🔍 Étapes pour reproduire
(Si c'est un bug)
1. Étape 1
2. Étape 2
3. Étape 3
4. Comportement attendu vs comportement observé

## ✅ Comportement attendu
Expliquez ce que vous attendiez qu'il se passe à l'origine.

## ❌ Comportement actuel
Expliquez ce qui se passe actuellement qui ne devrait pas se produire ou qui est une erreur.

## 🌐 Environnement
Décrivez l'environnement (infrastructure, environnement applicatif, autre...)

## 🎯 DoD (Definition of Done)
- [ ] Le code passe tous les tests automatisés.
- [ ] Le code a été revu par au moins une autre personne.
- [ ] La documentation (technique et utilisateur) a été mise à jour.
- [ ] Toutes les dépendances et impacts sont identifiés et traités.
- [ ] Le code a été déployé sur l'environnement de test et validé.
- [ ] Le client a accepté la solution (si applicable).

## ➕ Informations supplémentaires
Ajoutez ici toute autre information utile, comme des captures d'écran ou des logs.

## 💡 Suggestions de résolution
(Si applicable) Proposez des solutions possibles ou des idées pour résoudre le problème.